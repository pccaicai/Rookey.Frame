﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rookey.Frame.Mail
{
    /// <summary>
    /// 邮件接收参数
    /// </summary>
    public class MailReceiveParams : EmailParams
    {
        public MailReceiveParams(EmailType emailType, string username, string password, bool enableSSL = false)
            : base(emailType, username, password, enableSSL)
        {
        }
    }
}
