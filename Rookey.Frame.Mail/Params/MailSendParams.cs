﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rookey.Frame.Mail
{
    /// <summary>
    /// 邮件发送参数类
    /// </summary>
    public class MailSendParams : EmailParams
    {
        private string _from;

        public string From
        {
            get { return _from; }
        }
        private string _fromDisplay;

        public string FromDisplay
        {
            get { return _fromDisplay; }
        }

        private Dictionary<string, string> _to = null;

        public Dictionary<string, string> To
        {
            get { return _to; }
        }
        private Dictionary<string, string> _cc = null;

        public Dictionary<string, string> Cc
        {
            get { return _cc; }
        }
        private Dictionary<string, string> _bcc = null;

        public Dictionary<string, string> Bcc
        {
            get { return _bcc; }
        }
        private string _subject;

        public string Subject
        {
            get { return _subject; }
        }
        private string _body;

        public string Body
        {
            get { return _body; }
        }
        private List<string> _attachments = null;

        public List<string> Attachments
        {
            get { return _attachments; }
        }

        public MailSendParams(EmailType emailType, Dictionary<string, string> to, string from, string username, string password, string subject, string body = "", List<string> attachments = null, string fromDisplay = "", Dictionary<string, string> cc = null, Dictionary<string, string> bcc = null, bool enableSSL = false)
            : base(emailType, username, password, enableSSL)
        {
            this._from = string.IsNullOrEmpty(from) ? username : from;
            this._fromDisplay = fromDisplay;
            this._to = to;
            this._cc = cc;
            this._bcc = cc;
            this._subject = subject;
            this._body = body;
            this._attachments = attachments;
        }

        public MailSendParams(string server, int port, Dictionary<string, string> to, string from, string username, string password, string subject, string body = "", List<string> attachments = null, string fromDisplay = "", Dictionary<string, string> cc = null, Dictionary<string, string> bcc = null, bool enableSSL = false)
            : base(server, port, username, password, enableSSL)
        {
            this._from = string.IsNullOrEmpty(from) ? username : from;
            this._fromDisplay = fromDisplay;
            this._to = to;
            this._cc = cc;
            this._bcc = cc;
            this._subject = subject;
            this._body = body;
            this._attachments = attachments;
        }

        public MailSendParams(string server, int port, string to, string from, string username, string password, string subject, string body = "", List<string> attachments = null, string fromDisplay = "", string cc = null, string bcc = null, bool enableSSL = false)
            : base(server, port, username, password, enableSSL)
        {
            this._from = string.IsNullOrEmpty(from) ? username : from;
            this._fromDisplay = fromDisplay;
            if (!string.IsNullOrEmpty(to))
            {
                string[] token = to.Split(",".ToCharArray());
                if (token != null && token.Length > 0)
                {
                    Dictionary<string, string> dic = new Dictionary<string, string>();
                    foreach (string str in token)
                    {
                        dic.Add(str, string.Empty);
                    }
                    this._to = dic;
                }
            }
            if (!string.IsNullOrEmpty(cc))
            {
                string[] token = cc.Split(",".ToCharArray());
                if (token != null && token.Length > 0)
                {
                    Dictionary<string, string> dic = new Dictionary<string, string>();
                    foreach (string str in token)
                    {
                        dic.Add(str, string.Empty);
                    }
                    this._cc = dic;
                }
            }
            if (!string.IsNullOrEmpty(bcc))
            {
                string[] token = bcc.Split(",".ToCharArray());
                if (token != null && token.Length > 0)
                {
                    Dictionary<string, string> dic = new Dictionary<string, string>();
                    foreach (string str in token)
                    {
                        dic.Add(str, string.Empty);
                    }
                    this._bcc = dic;
                }
            }
            this._subject = subject;
            this._body = body;
            this._attachments = attachments;
        }
    }
}
