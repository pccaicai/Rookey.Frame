﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rookey.Frame.Mail
{
    /// <summary>
    /// 邮件参数
    /// </summary>
    public class EmailParams
    {
        public EmailParams(EmailType emailType, string username, string password, bool enableSSL = false)
        {
            this._emailType = emailType;
            this._username = username;
            this._password = password;
            this._enabledSsl = enableSSL;
        }

        public EmailParams(string server, int port, string username, string password, bool enableSSL = false)
        {
            this._server = server;
            this._port = port;
            this._username = username;
            this._password = password;
            this._enabledSsl = enableSSL;
        }

        private string _server;

        public string Server
        {
            get { return _server; }
            set { _server = value; }
        }

        private int _port;

        public int Port
        {
            get { return _port; }
            set { _port = value; }
        }

        private EmailType _emailType = EmailType.None;

        public EmailType EmailType
        {
            get { return _emailType; }
        }
        private bool _enabledSsl = false;

        public bool EnabledSsl
        {
            get { return _enabledSsl; }
        }
        private string _username;

        public string Username
        {
            get { return _username; }
        }
        private string _password;

        public string Password
        {
            get { return _password; }
        }
    }
}
