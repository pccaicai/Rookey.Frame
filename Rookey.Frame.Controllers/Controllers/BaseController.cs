﻿/*----------------------------------------------------------------
        // Copyright (C) 2016 Rookey
        // 版权所有
        // 开发者：Rookey
        // Email：rookey@yeah.net
        // QQ：3319549098
//----------------------------------------------------------------*/

using Rookey.Frame.Controllers.Attr;
using System.Web.Mvc;
using Rookey.Frame.Base;

namespace Rookey.Frame.Controllers
{
    /// <summary>
    /// 控制器基类
    /// </summary>
    [PermissionFilter]
    public class BaseController : Controller
    {
        /// <summary>
        /// 当前用户
        /// </summary>
        public UserInfo CurrentUser
        {
            get { return UserInfo.CurrentUserInfo; }
        }
    }
}