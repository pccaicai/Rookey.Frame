﻿/*----------------------------------------------------------------
        // Copyright (C) 2016 Rookey
        // 版权所有
        // 开发者：Rookey
        // Email：rookey@yeah.net
        // QQ：3319549098
//----------------------------------------------------------------*/

using Rookey.Frame.Controllers.Attr;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Http;

namespace Rookey.Frame.Controllers
{
    /// <summary>
    /// Api控制器基类
    /// </summary>
    [PermissionFilter]
    public class BaseApiController : ApiController
    {
    }
}
