﻿/*----------------------------------------------------------------
        // Copyright (C) 2016 Rookey
        // 版权所有
        // 开发者：Rookey
        // Email：rookey@yeah.net
        // QQ：3319549098
//----------------------------------------------------------------*/

using System.Collections.Generic;
using System.Web.Mvc;
using Rookey.Frame.Common;
using Rookey.Frame.Model.OrgM;
using Rookey.Frame.Operate.Base;
using System.Web;
using System.Threading.Tasks;
using Rookey.Frame.Controllers.Attr;
using System;

namespace Rookey.Frame.Controllers.OrgM
{
    /// <summary>
    /// 组织机构相关操作控制器（异步）
    /// </summary>
    public class OrgMAsyncController : AsyncBaseController
    {
        /// <summary>
        /// 异步获取部门职务
        /// </summary>
        /// <returns></returns>
        [OpTimeMonitor]
        public Task<ActionResult> GetDeptDutysAsync()
        {
            return Task.Factory.StartNew(() =>
            {
                return new OrgMController(Request).GetDeptDutys();
            }).ContinueWith<ActionResult>(task =>
            {
                return task.Result;
            });
        }
    }

    /// <summary>
    /// 组织机构相关操作控制器
    /// </summary>
    public class OrgMController : BaseController
    {
        #region 构造函数

        private HttpRequestBase _Request = null; //请求对象

        /// <summary>
        /// 无参构造函数
        /// </summary>
        public OrgMController()
        {
            _Request = Request;
        }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="request">请求对象</param>
        public OrgMController(HttpRequestBase request)
        {
            _Request = request;
        }

        #endregion

        /// <summary>
        /// 获取部门职务
        /// </summary>
        /// <returns></returns>
        [OpTimeMonitor]
        public ActionResult GetDeptDutys()
        {
            if (_Request == null) _Request = Request;
            Guid deptId = _Request["deptId"].ObjToGuid();
            List<OrgM_Duty> dutys = OrgMOperate.GetDeptDutys(deptId);
            dutys.Insert(0, new OrgM_Duty() { Id = Guid.Empty, Name = "请选择" });
            return Json(dutys, JsonRequestBehavior.AllowGet);
        }
    }
}
