﻿/*----------------------------------------------------------------
        // Copyright (C) 2016 Rookey
        // 版权所有
        // 开发者：Rookey
        // Email：rookey@yeah.net
        // QQ：3319549098
//----------------------------------------------------------------*/

using System;
using System.Linq;
using System.Web.Mvc;
using Rookey.Frame.Common;
using System.Web;
using Rookey.Frame.Operate.Base;
using Rookey.Frame.Base;
using Rookey.Frame.Controllers.Attr;
using Rookey.Frame.Operate.Base.TempModel;
using Rookey.Frame.Model.Sys;
using Rookey.Frame.Operate.Base.Extension;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Net.Http;
using Rookey.Frame.Controllers.Other;
using System.Web.Security;

namespace Rookey.Frame.Controllers
{
    /// <summary>
    /// 用户控制器（异步）
    /// </summary>
    public class UserAsyncController : AsyncBaseController
    {
        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="username">用户名</param>
        /// <param name="userpwd">密码</param>
        /// <param name="valcode">验证码</param>
        /// <returns></returns>
        [Login]
        [Anonymous]
        public Task<ActionResult> UserLoginAsync(string username, string userpwd, string valcode)
        {
            return Task.Factory.StartNew(() =>
            {
                return new UserController(Request, Response, Session, TempData).UserLogin(username, userpwd, valcode);
            }).ContinueWith<ActionResult>(task =>
            {
                return task.Result;
            });
        }

        /// <summary>
        /// 获取数据权限组织树
        /// </summary>
        /// <returns></returns>
        public Task<ActionResult> GetDataPermissionOrgTreeAsync()
        {
            return Task.Factory.StartNew(() =>
            {
                return new UserController().GetDataPermissionOrgTree();
            }).ContinueWith<ActionResult>(task =>
            {
                return task.Result;
            });
        }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <returns></returns>
        public Task<ActionResult> ChangePwdAsync()
        {
            return Task.Factory.StartNew(() =>
            {
                return new UserController(Request).ChangePwd();
            }).ContinueWith<ActionResult>(task =>
            {
                return task.Result;
            });
        }
    }

    /// <summary>
    /// 用户控制器
    /// </summary>
    public class UserController : BaseController
    {
        #region 构造函数

        private HttpSessionStateBase _Session = null; //Session
        private HttpRequestBase _Request = null; //请求对象
        private HttpResponseBase _Response = null; //响应对象
        private TempDataDictionary _TempData = null; //临时字典对象

        /// <summary>
        /// 无参构造函数
        /// </summary>
        public UserController()
        {
            _Request = Request;
            _Session = Session;
            _Response = Response;
            _TempData = TempData;
        }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="request">请求对象</param>
        public UserController(HttpRequestBase request)
        {
            _Request = request;
        }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="request">请求对象</param>
        /// <param name="response">返回请求</param>
        /// <param name="session">session</param>
        public UserController(HttpRequestBase request, HttpResponseBase response, HttpSessionStateBase session, TempDataDictionary tempData)
        {
            _Request = request;
            _Session = session;
            _Response = response;
            _TempData = tempData;
        }

        #endregion

        #region 公共方法

        private const string LOGINERROR = "LoginError";

        /// <summary>
        /// 登录页面
        /// </summary>
        /// <returns></returns>
        [Anonymous]
        public ActionResult Login()
        {
            if (!ToolOperate.IsNeedInit()) //不需要初始化时
            {
                if (WebConfigHelper.GetAppSettingValue("NeedRepairTable") == "true") //需要修复数据表
                {
                    string tables = WebConfigHelper.GetAppSettingValue("RepairTables"); //要修复的数据表
                    List<string> token = new List<string>();
                    if (!string.IsNullOrEmpty(tables))
                    {
                        token = tables.Split(",".ToCharArray()).ToList();
                    }
                    ToolOperate.RepairTables(token);
                }
            }
            else //需要初始化
            {
                return RedirectToAction("Init", "Page");
            }
            ViewBag.IsShowValidateCode = (Convert.ToInt32(Session[LOGINERROR]) >= 2).ToString().ToLower();
            return View();
        }

        /// <summary>
        /// 弹出登录框
        /// </summary>
        /// <returns></returns>
        [Anonymous]
        public ActionResult DialogLogin()
        {
            ViewBag.IsShowValidateCode = (Convert.ToInt32(Session[LOGINERROR]) >= 2).ToString().ToLower();
            return View();
        }

        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="username">用户名</param>
        /// <param name="userpwd">密码</param>
        /// <param name="valcode">验证码</param>
        /// <returns></returns>
        [Login]
        [Anonymous]
        public ActionResult UserLogin(string username, string userpwd, string valcode)
        {
            if (_Request == null) _Request = Request;
            if (_Response == null) _Response = Response;
            if (_Session == null) _Session = Session;
            string errMsg = string.Empty;
            ViewBag.IsShowValidateCode = "false";
            bool isNoCode = _Request["isNoCode"].ObjToBool(); //是否不需要验证码
            if (_Session[LOGINERROR] != null && !isNoCode && Convert.ToInt32(_Session[LOGINERROR]) >= 2)
            {
                bool validatecode = false;
                if (_TempData.ContainsKey(SecurityController.VALIDATECODE))
                {
                    string code = _TempData[SecurityController.VALIDATECODE].ToString();
                    validatecode = valcode == code;
                }
                if (!validatecode)
                {
                    return Json(new LoginReturnResult() { Success = false, Message = "验证码错误！", IsShowCode = true });
                }
            }
            //获取用户信息
            UserInfo userInfo = UserOperate.GetUserInfo(username, userpwd, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                var isShowCode = false;
                _Session[LOGINERROR] = _Session[LOGINERROR] == null ? 0 : Convert.ToInt32(_Session[LOGINERROR]) + 1;
                if (!isNoCode && Convert.ToInt32(_Session[LOGINERROR]) >= 2)
                {
                    ViewBag.IsShowValidateCode = "true";
                    isShowCode = true;
                }
                return Json(new LoginReturnResult() { Success = false, Message = errMsg, IsShowCode = isShowCode });
            }
            else
            {
                _Session[LOGINERROR] = null;
                //客户端浏览器参数
                int w = _Request["w"].ObjToInt();
                int h = _Request["h"].ObjToInt();
                userInfo.ClientBrowserWidth = w;
                userInfo.ClientBrowserHeight = h;
                //获取客户端IP
                userInfo.ClientIP = WebHelper.GetClientIP(_Request);
                //用户票据保存
                FormsPrincipal.Login(userInfo.UserName, userInfo, UserInfo.ACCOUNT_EXPIRATION_TIME);
                //登录成功写cookie，保存客户端用户名
                var userNameCookie = new HttpCookie("UserName", userInfo.UserName);
                userNameCookie.Expires = DateTime.Now.AddDays(365);
                _Response.Cookies.Add(userNameCookie);
                //执行登录成功后的操作
                CommonOperate.ExecuteUserOperateHandleMethod("AfterLoginSuccess", new object[] { _Session, _Request, _Response, username, userpwd, 30 });

                return Json(new LoginReturnResult() { Success = true, Message = string.Empty, Url = HttpUtility.UrlEncode(string.Empty) });
            }
        }

        /// <summary>
        /// 登出
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout()
        {
            FormsPrincipal.Logout();
            Response.Cookies.Remove(FormsAuthentication.FormsCookieName);
            Session.Remove("UserName");
            return RedirectToAction("Login");
        }

        /// <summary>
        /// 获取数据权限组织树
        /// </summary>
        /// <returns></returns>
        public ActionResult GetDataPermissionOrgTree()
        {
            TreeNode tempNode = new TreeNode() { id = "-1", text = "全部", iconCls = "eu-icon-dept" };
            TreeNode currDeptNode = new TreeNode() { id = "0", text = "本部门", iconCls = "eu-icon-dept" };
            TreeNode node = CommonOperate.GetTreeNode<Sys_Organization>(null, null, null, null, null, "eu-icon-dept");
            List<TreeNode> list = new List<TreeNode> { tempNode, currDeptNode };
            if (node != null) list.Add(node);
            return Json(list.ToJson().Content);
        }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <returns></returns>
        public ActionResult ChangePwd()
        {
            UserInfo userInfo = UserInfo.CurrentUserInfo;
            if (userInfo == null)
            {
                return Json(new ReturnResult() { Success = false, Message = "您未登录系统或登录时间过长，请重新登录系统后再修改密码！" });
            }
            string errMsg = string.Empty;
            string oldPwd = _Request["oldPwd"].ObjToStr();
            string newPwd = _Request["newPwd"].ObjToStr();
            UserInfo tempUserInfo = UserOperate.GetUserInfo(userInfo.UserName, oldPwd, out errMsg);
            if (tempUserInfo == null)
            {
                return Json(new ReturnResult() { Success = false, Message = "您当前登录密码输入不正确，请重新输入！" });
            }
            bool rs = UserOperate.ModifyPassword(userInfo.UserId, newPwd, out errMsg);
            if (rs)
            {
                CommonOperate.ExecuteUserOperateHandleMethod("AfterChangePwd", new object[] { userInfo.UserName, oldPwd, newPwd });
            }
            return Json(new ReturnResult() { Success = rs, Message = errMsg });
        }

        #endregion
    }

    /// <summary>
    /// 用户API控制器
    /// </summary>
    public class UserApiController : BaseApiController
    {
        #region 公共方法

        /// <summary>
        /// 获取数据权限组织树
        /// </summary>
        /// <returns></returns>
        [System.Web.Http.HttpGet]
        [System.Web.Http.HttpPost]
        public dynamic GetDataPermissionOrgTree()
        {
            HttpRequestBase request = WebHelper.GetContextRequest(Request);
            JsonResult result = new UserController(request).GetDataPermissionOrgTree() as JsonResult;
            return result.Data;
        }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <returns></returns>
        [System.Web.Http.HttpGet]
        [System.Web.Http.HttpPost]
        public ReturnResult ChangePwd()
        {
            HttpRequestBase request = WebHelper.GetContextRequest(Request);
            JsonResult result = new UserController(request).ChangePwd() as JsonResult;
            return result.Data as ReturnResult;
        }

        #endregion
    }
}
