﻿using Rookey.Frame.Base;
using Rookey.Frame.Controllers.Attr;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace Rookey.Frame.Controllers
{
    /// <summary>
    /// 异步基础控制器
    /// </summary>
    [PermissionFilter]
    public class AsyncBaseController : AsyncController
    {
        /// <summary>
        /// 当前用户
        /// </summary>
        public UserInfo CurrentUser
        {
            get { return UserInfo.CurrentUserInfo; }
        }
    }
}
