﻿/*----------------------------------------------------------------
        // Copyright (C) 2016 Rookey
        // 版权所有
        // 开发者：Rookey
        // Email：rookey@yeah.net
        // QQ：3319549098
//----------------------------------------------------------------*/

using Rookey.Frame.AutoProcess;
using Rookey.Frame.Operate.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rookey.Frame.Controllers.AutoHandle
{
    /// <summary>
    /// 系统自动任务
    /// </summary>
    public class SysAutoHandle
    {
        /// <summary>
        /// 添加后台系统任务
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="e"></param>
        public static void SysBackgroundTaskAdd(object obj, EventArgs e)
        {
            BackgroundTask reBuildIndexTask = new BackgroundTask((args) =>
            {
                SystemOperate.RebuildAllTableIndex();
                return true;
            }, null, false, 60, true, Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd") + " 05:30"));
            AutoProcessTask.AddTask(reBuildIndexTask);
        }
    }
}
