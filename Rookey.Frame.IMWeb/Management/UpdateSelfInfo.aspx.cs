﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Rookey.Frame.IMCore;
using Rookey.Frame.IMCore.IO;

public partial class Lesktop_Management_UpdateSelfInfo : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{
		CommandCtrl cmdCtrl = FindControl("CommandCtrl1") as CommandCtrl;
		cmdCtrl.OnCommand+=new CommandCtrl.OnCommandDelegate(cmdCtrl_OnCommand);

		cmdCtrl.State["SelfInfo"] = Rookey.Frame.IMCore.ServerImpl.Instance.GetCurrentUser(Context).DetailsJson;
	}

	private void cmdCtrl_OnCommand(string command, object data)
	{
		CommandCtrl cmdCtrl = FindControl("CommandCtrl1") as CommandCtrl;
		try
		{
			AccountInfo cu = Rookey.Frame.IMCore.ServerImpl.Instance.GetCurrentUser(Context);
			if (command == "Update")
			{
				Hashtable info = data as Hashtable;
				if (Request.Files["file_headimg"] != null && Request.Files["file_headimg"].InputStream.Length > 0)
				{
					String filename = String.Format("/{0}/pub/{1}{2}", cu.Name, Guid.NewGuid().ToString().Replace("-", ""), Path.GetExtension(Request.Files["file_headimg"].FileName));
					info["HeadIMG"] = filename;
					Directory.CreateDirectory(Path.GetDirectoryName(filename));
					using (System.IO.Stream stream = File.Open(filename, System.IO.FileMode.Create, System.IO.FileAccess.Write, System.IO.FileShare.None))
					{
						byte[] buffer = new byte[4 * 1024];
						while (true)
						{
							int c = Request.Files["file_headimg"].InputStream.Read(buffer, 0, buffer.Length);
							if (c == 0) break;
							stream.Write(buffer, 0, c);
						}
					}
				}

				AccountImpl.Instance.UpdateUserInfo(cu.Name, info);

				foreach (string friend in cu.Friends)
				{
					AccountInfo friendInfo = AccountImpl.Instance.GetUserInfo(friend);
					if (friendInfo.Type == 0)
					{
						SessionManagement.Instance.Send(friend, "GLOBAL:REFRESH_FIRENDS", null);
					}
				}
				SessionManagement.Instance.Send(cu.Name, "GLOBAL:REFRESH_FIRENDS", null);
				cmdCtrl.State["SelfInfo"] = cu.DetailsJson;
				cmdCtrl.State["Action"] = "ResetUserInfo";
			}
			else if (command == "ChangePassword")
			{
				Hashtable info = data as Hashtable;
				AccountImpl.Instance.UpdateUserInfo(cu.Name, info);
			}
		}
		catch (Exception ex)
		{
			cmdCtrl.State["Action"] = "Alert";
			cmdCtrl.State["Message"] = ex.Message;
		}
	}
}
