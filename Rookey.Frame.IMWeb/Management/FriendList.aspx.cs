﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Text;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Rookey.Frame.IMCore;

public partial class Management_FriendList : System.Web.UI.Page
{
	static string RowFormat =
	@"
	<tr>
		<td class='headimg'>&nbsp;</td>
		<td class='name'>{1}</td>
		<td class='nickname'>{2}</td>
		<td class='email'>{3}</td>
		<td class='operation'><a href='javascript:Delete({4},{5},{6})'>删除好友</a></td>
	</tr>
	";

	protected void Page_Load(object sender, EventArgs e)
	{
		CommandCtrl cmdCtrl = FindControl("CommandCtrl") as CommandCtrl;
		cmdCtrl.OnCommand += new CommandCtrl.OnCommandDelegate(cmdCtrl_OnCommand);

		cmdCtrl.State["Action"] = null;
	}

	private void cmdCtrl_OnCommand(string command, object data)
	{
		string peer = Convert.ToString(data);
		AccountInfo cu = ServerImpl.Instance.GetCurrentUser(Context);

		if (command == "Delete")
		{
			AccountImpl.Instance.DeleteFriend(cu.Name, peer);

			string content = Utility.RenderHashJson(
				"Type", "DeleteFriendNotify",
				"User", AccountImpl.Instance.GetUserInfo(cu.Name),
				"Peer", AccountImpl.Instance.GetUserInfo(peer),
				"Info", ""
			);
			MessageImpl.Instance.NewMessage(peer, "administrator", content, null);
			MessageImpl.Instance.NewMessage(cu.Name, "administrator", content, null);
		}
	}

	protected String RenderFriendList()
	{
		AccountInfo cu = AccountImpl.Instance.GetUserInfo(ServerImpl.Instance.GetUserName(Context));
		StringBuilder builder = new StringBuilder();
		foreach (string name in AccountImpl.Instance.GetUserInfo(cu.Name).Friends)
		{
			AccountInfo fi = AccountImpl.Instance.GetUserInfo(name);
			if (fi.Type == 0 && fi.Name.ToLower() != "sa")
			{
				builder.AppendFormat(
					RowFormat, "", 
					HtmlUtil.ReplaceHtml(fi.Name), 
					HtmlUtil.ReplaceHtml(fi.Nickname), 
					HtmlUtil.ReplaceHtml(fi.EMail), 
					fi.ID,
					Rookey.Frame.IMCore.Utility.RenderJson(fi.Nickname),
					Rookey.Frame.IMCore.Utility.RenderJson(fi.Name)
				);
			}
		}
		return builder.ToString();
	}
}
