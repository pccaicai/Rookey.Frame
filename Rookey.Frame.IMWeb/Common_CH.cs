﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Rookey.Frame.IMCore;

class Common_CH : CommandHandler
{
	public Common_CH(HttpContext context, String sessionId, String id, String data)
		: base(context, sessionId, id, data)
	{
		
	}

	public override string Process()
	{
		Hashtable ps = Rookey.Frame.IMCore.Utility.ParseJson(Data as string) as Hashtable;
		string action = ps["Action"] as string;
		if (action == "GetFriends")
		{
			List<AccountInfo.Details> fis = new List<AccountInfo.Details>();
			foreach (string f in AccountImpl.Instance.GetUserInfo(UserName).Friends)
			{
				fis.Add(AccountImpl.Instance.GetUserInfo(f).DetailsJson);
			}

			return Utility.RenderHashJson("Friends", fis);
		}
		else if (action == "SendAddFriendRequest")
		{
			string peer = ps["Peer"] as string;

			if (String.Compare(peer, UserName, true) == 0)
			{
				throw new Exception("不能添加自己为好友！");
			}

			if (AccountImpl.Instance.GetUserInfo(peer) == null)
			{
				throw new Exception(String.Format("用户(或群组) \"{0}\" 不存在！", peer));
			}

			AccountInfo peerInfo = AccountImpl.Instance.GetUserInfo(peer);
			if (peerInfo.Type == 0)
			{
				if (AccountImpl.Instance.GetUserInfo(UserName).ContainsFriend(peer))
				{
					throw new Exception(String.Format("用户 \"{0}({1})\" 已经是您的好友！", peerInfo.Nickname, peer));
				}

				MessageImpl.Instance.NewMessage(
					peer, "administrator",
					Utility.RenderHashJson("Type", "AddFriendRequest", "Peer", AccountImpl.Instance.GetUserInfo(UserName), "Info", ps["Info"] as string),
					null
				);
			}
			else
			{
				if (AccountImpl.Instance.GetUserInfo(UserName).ContainsFriend(peer))
				{
					throw new Exception(String.Format("您已加入群 \"{0}({1})\"！", peerInfo.Nickname, peer));
				}

				MessageImpl.Instance.NewMessage(
					peerInfo.Creator, "administrator",
					Utility.RenderHashJson(
						"Type", "AddGroupRequest",
						"User", AccountImpl.Instance.GetUserInfo(UserName),
						"Group", peerInfo, 
						"Info", ps["Info"] as string
					),
					null
				);
			}

            return Utility.RenderHashJson("Result", true);
		}
		else if (action == "DeleteFriend")
		{
			string peer = ps["Peer"] as string;

			AccountImpl.Instance.DeleteFriend(UserName, peer);

			string content = Utility.RenderHashJson(
				"Type", "DeleteFriendNotify",
				"User", AccountImpl.Instance.GetUserInfo(UserName),
				"Peer", AccountImpl.Instance.GetUserInfo(peer),
				"Info", ps["Info"] as string
			);
			MessageImpl.Instance.NewMessage(peer, "administrator", content, null);
			MessageImpl.Instance.NewMessage(UserName, "administrator", content, null);

			return Utility.RenderHashJson("Result", true);
		}
		else if (action == "AddFriend")
		{
			string peer = ps["Peer"] as string;

			if (!AccountImpl.Instance.GetUserInfo(UserName).ContainsFriend(peer))
			{
				AccountImpl.Instance.AddFriend(UserName, peer);

				string content = Utility.RenderHashJson(
					"Type", "AddFriendNotify",
					"User", AccountImpl.Instance.GetUserInfo(UserName),
					"Peer", AccountImpl.Instance.GetUserInfo(peer),
					"Info", ps["Info"] as string
				);
				MessageImpl.Instance.NewMessage(peer, "administrator", content, null);
				MessageImpl.Instance.NewMessage(UserName, "administrator", content, null);
			}
			return Utility.RenderHashJson("Result", true);
		}
		else if (action == "AddToGroup")
		{
			string user = ps["User"] as string;
			string group = ps["Group"] as string;

			AccountImpl.Instance.AddFriend(user, group);

			string content = Utility.RenderHashJson(
				"Type", "AddToGroupNotify",
				"User", AccountImpl.Instance.GetUserInfo(user),
				"Group", AccountImpl.Instance.GetUserInfo(group)
			);
			MessageImpl.Instance.NewMessage(group, "administrator", content, null);

			return Utility.RenderHashJson("Result", true);
		}
		else if (action == "GetAccountInfo")
		{
			string name = ps["Name"] as string;

			return Utility.RenderHashJson("Info", AccountImpl.Instance.GetUserInfo(name));
		}
		else if (action == "FindHistory")
		{
			MessageImpl.Instance.WriteCache();
			List<Message> msgs = MessageImpl.Instance.FindHistory(
				ps["User"].ToString(), ps["Peer"].ToString(),
				Convert.ToDateTime(ps["From"]), Convert.ToDateTime(ps["To"])
			);
			return Utility.RenderHashJson(
				"Result", true,
				"User", ps["User"],
				"Peer", ps["Peer"],
				"Messages", msgs
			);
		}
		else if (action == "GetGroupMembers")
		{
			string name = ps["Name"].ToString();
			AccountInfo groupInfo = AccountImpl.Instance.GetUserInfo(name);
			List<AccountInfo.Details> members = new List<AccountInfo.Details>();
			foreach (string memberName in groupInfo.Friends)
			{
				members.Add(AccountImpl.Instance.GetUserInfo(memberName).DetailsJson);
			}
			return Utility.RenderHashJson(
				"Result", true,
				"Members", members,
				"GroupInfo", groupInfo,
				"GroupCreator", AccountImpl.Instance.GetUserInfo(groupInfo.Creator).DetailsJson
			);
		}
		else if (action == "ReLogin")
		{
			SessionManagement.Instance.GetAccountState(UserName).NewSession(ps["SessionID"].ToString());

			return Utility.RenderHashJson("Result", true);
		}

		throw new NotImplementedException();
	}

	public override void Process(object data)
	{
		throw new NotImplementedException();
	}
}