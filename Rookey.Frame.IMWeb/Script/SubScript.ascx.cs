﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;

public partial class Script_SubScript : System.Web.UI.UserControl
{
	static String SkinCssFormat =
	"<link href=\"{0}/Themes/Default/skin.css\" rel=\"stylesheet\" type=\"text/css\" />\r\n";

	static String ScriptFormat =
	"<script src=\"{0}/Core/Common.js\" type=\"text/javascript\"></script>\r\n" +
	"<script src=\"{0}/Core/Config.js.aspx\" type=\"text/javascript\"></script>\r\n" +
	"<script src=\"{0}/Core/Extent.js\" type=\"text/javascript\"></script>\r\n" +
	"<script src=\"{0}/Core/UI.js\" type=\"text/javascript\"></script>\r\n" +
	"<script src=\"{0}/Core/Sub.js\" type=\"text/javascript\"></script>\r\n";

	protected void Page_Load(object sender, EventArgs e)
	{

	}

	protected String CommonScript
	{
		get
		{
			string res = Rookey.Frame.IMCore.ServerImpl.Instance.ServiceUrl;
			if (!res.EndsWith("/")) res += "/";
            res += Rookey.Frame.IMCore.ServerImpl.Instance.ResPath;
			return String.Format("\r\n" + SkinCssFormat + ScriptFormat, res);
		}
	}
}
