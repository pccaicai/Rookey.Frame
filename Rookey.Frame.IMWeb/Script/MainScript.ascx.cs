﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;

public partial class Script_MainScript : System.Web.UI.UserControl
{
	static String ScriptFormat =
	"<link href=\"{0}/Themes/Default/Desktop/Desktop.css\" rel=\"stylesheet\" type=\"text/css\" />\r\n" +
	"<script src=\"{0}/Core/Common.js\" type=\"text/javascript\"></script>\r\n" +
	"<script src=\"{0}/Core/Config.js.aspx\" type=\"text/javascript\"></script>\r\n" +
	"<script src=\"{0}/Core/Extent.js\" type=\"text/javascript\"></script>\r\n" +
	"<script src=\"{0}/Core/Main.js\" type=\"text/javascript\"></script>\r\n" +
	"<script src=\"{0}/Core/Main/Desktop.js\" type=\"text/javascript\"></script>\r\n" +
	"<script src=\"{0}/Core/Main/Window.js\" type=\"text/javascript\"></script>\r\n";

	protected void Page_Load(object sender, EventArgs e)
	{

	}

	protected String CommonScript
	{
		get
		{
			string res = Rookey.Frame.IMCore.ServerImpl.Instance.ServiceUrl;
			if (!res.EndsWith("/")) res += "/";
			res += Rookey.Frame.IMCore.ServerImpl.Instance.ResPath; 
			return String.Format(ScriptFormat, res);
		}
	}
}
