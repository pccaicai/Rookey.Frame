﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Rookey.Frame.IMCore;

public partial class Upload : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{
		if (IsPostBack)
		{
			try
			{
				FileUpload fu=FindControl("UploadFile") as FileUpload;
				string name = System.IO.Path.GetFileName(fu.FileName);

				String filename = Rookey.Frame.IMCore.ServerImpl.Instance.GetFullPath(Context, "Temp") + "/" + Guid.NewGuid().ToString();
				Rookey.Frame.IMCore.IO.Directory.CreateDirectory(filename);
				filename += "/" + name;

				using (System.IO.Stream stream = Rookey.Frame.IMCore.IO.File.Create(filename))
				{
					try
					{
						byte[] buffer = new byte[16 * 1024];
						int count = 0;
						do
						{
							count = fu.FileContent.Read(buffer, 0, buffer.Length);
							stream.Write(buffer, 0, count);
						} while (count == buffer.Length);
					}
					finally
					{
						stream.Close();
					}
				}

				(FindControl("data") as HtmlInputHidden).Value = Rookey.Frame.IMCore.Utility.RenderHashJson(
					"Result", true,
					"Path", filename
				);
			}
			catch (System.Exception ex)
			{
				(FindControl("data") as HtmlInputHidden).Value = Rookey.Frame.IMCore.Utility.RenderHashJson(
					"Result", false,
					"Exception", ex
				);
			}
		}
	}
}
