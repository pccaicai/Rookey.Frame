﻿using System;
using System.Collections;
using System.Data;
using System.Configuration;
using System.Web;
using System.Xml;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Rookey.Frame.IMCore.IO;
using System.Threading;
using Rookey.Frame.IMCore;

namespace Rookey.Frame.IMWeb
{
	class SendFileHandler : IHttpHandler
	{

		public SendFileHandler()
		{
		}

		void IHttpHandler.ProcessRequest(HttpContext context)
		{
			Exception error = null;

			HttpPostedFile file = context.Request.Files[0];

			String filename = Rookey.Frame.IMCore.ServerImpl.Instance.GetFullPath(context, "Temp") + "/" + Guid.NewGuid().ToString();
			Rookey.Frame.IMCore.IO.Directory.CreateDirectory(filename);
			filename += "/" + file.FileName;

			try
			{
				file.SaveAs(ServerImpl.Instance.MapPath(filename));
			}
			catch(Exception e)
			{
				error = e;
			}

			if (error == null) context.Response.Write(Rookey.Frame.IMCore.Utility.RenderHashJson("Result", true, "Path", filename));
			else context.Response.Write(Rookey.Frame.IMCore.Utility.RenderHashJson("Result", false, "Exception", error));
		}

		bool IHttpHandler.IsReusable
		{
			get { return true; }
		}
	}
}
