﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Rookey.Frame.IMCore;

public class WebIM : CommandHandler
{
	public WebIM(HttpContext context, String sessionId, String id, String data)
		: base(context, sessionId, id, data)
	{
	}

	public override string Process()
	{
        Hashtable param = Utility.ParseJson(Data) as Hashtable;

		String action = param["Action"] as String;
		if (action == "NewMessage")
		{
#			if DEBUG
			System.Threading.Thread.Sleep(1000);
#			endif
			String receiver = param["Receiver"] as String;
			String sender = param["Sender"] as String;
			String content = param["Content"] as String;

			Message msg = MessageImpl.Instance.NewMessage(receiver, sender, content, param);
			return Utility.RenderJson(msg);
		}
		throw new NotImplementedException();
	}

	public override void Process(object data)
	{
		throw new NotImplementedException();
	}
}
