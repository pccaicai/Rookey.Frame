﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Web;
using System.Net;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Net.Json;

namespace Rookey.Frame.IMClient
{

	public class CommandHandler
	{
		public String CommandID;
		public object Hanlder;
		public Control Browser;

		public CommandHandler(String cid, object handler, Control b)
		{
			CommandID = cid;
			Hanlder = handler;
			Browser = b;
		}
	}

	delegate void CommandHandlerCallbackDelegate(CommandHandler ch, object data);

	[System.Runtime.InteropServices.ComVisibleAttribute(true)]
	public class ReceiveResponseHandler
	{
		Control m_Browser = null;

		public ReceiveResponseHandler(Control b)
		{
			m_Browser = b;
		}

		public void Start()
		{
			lock (m_Lock)
			{
				if (m_Thread == null)
				{
					m_Thread = new ReceiveResponseThread();
					m_Thread.Start();
				}
			}
		}

		public void Stop()
		{
			lock (m_Lock)
			{
				if (m_Thread != null)
				{
					m_Thread.Stop();
					m_Thread = null;
				}
			}
		}

		public bool IsRunning()
		{
			return m_Thread != null && m_Thread.IsRunning;
		}

		static object m_Lock = new object();
		static Hashtable m_Handlers = new Hashtable();
		static ReceiveResponseThread m_Thread = null;

		public void NewCommandHandler(string commandId, object handler)
		{
			lock (m_Handlers)
			{
				m_Handlers[commandId] = new CommandHandler(commandId, handler, m_Browser);
			}
		}

		public void RemoveCommandHandler(string commandId)
		{
			lock (m_Handlers)
			{
				m_Handlers.Remove(commandId);
			}
		}

		public void InvokeCallback(string commandId, object data)
		{
			lock (m_Handlers)
			{
				if (m_Handlers.ContainsKey(commandId))
				{
					CommandHandler ch = m_Handlers[commandId] as CommandHandler;
					if (ch.Browser.IsHandleCreated && !ch.Browser.IsDisposed)
					{
						ch.Browser.BeginInvoke(new CommandHandlerCallbackDelegate(InvokeHandlerCallback), ch, data);
					}
					m_Handlers.Remove(commandId);
				}
			}
		}

		public void InvokeErrorCallback(string commandId, object data)
		{
			lock (m_Handlers)
			{
				if (commandId == "all")
				{
					foreach (DictionaryEntry ent in m_Handlers)
					{
						CommandHandler ch = ent.Value as CommandHandler;

						if (ch.Browser.IsHandleCreated && !ch.Browser.IsDisposed)
						{
							ch.Browser.BeginInvoke(new CommandHandlerCallbackDelegate(InvokeHandlerErrorCallback), ch, data);
						}
					}
					m_Handlers = new Hashtable();
				}
				else
				{
					if (m_Handlers.ContainsKey(commandId))
					{
						CommandHandler ch = m_Handlers[commandId] as CommandHandler;
						if (ch.Browser.IsHandleCreated && !ch.Browser.IsDisposed)
						{
							ch.Browser.BeginInvoke(new CommandHandlerCallbackDelegate(InvokeHandlerErrorCallback), ch, data);
						}
						m_Handlers.Remove(commandId);
					}
				}
			}
		}

		static void InvokeHandlerCallback(CommandHandler ch, object data)
		{
			if (data.GetType().FullName == "System.__ComObject")
			{
				Utility.InvokeMethod(ch.Hanlder, "Callback", data, "data");
			}
			else
			{
				Utility.InvokeMethod(ch.Hanlder, "Callback", Utility.RenderJson(data), "json");
			}
		}


		static void InvokeHandlerErrorCallback(CommandHandler ch, object data)
		{
			if (data.GetType().FullName == "System.__ComObject")
			{
				Utility.InvokeMethod(ch.Hanlder, "ErrorCallback", data, "data");
			}
			else
			{
				Utility.InvokeMethod(ch.Hanlder, "ErrorCallback", Utility.RenderJson(data), "json");
			}
		}

		static ReceiveResponseHandler m_Global = new ReceiveResponseHandler(null);

		public static ReceiveResponseHandler Global
		{
			get { return ReceiveResponseHandler.m_Global; }
		}


	}

	public class ReceiveResponseThread
	{
		Thread m_Thread = null;
		Boolean m_Stop = true;
		AutoResetEvent m_Event = null;

		public ReceiveResponseThread()
		{
			m_Thread = new Thread(ThreadEntry);
		}

		public bool IsRunning
		{
			get
			{
				return !m_Stop;
			}
		}

		public void Start()
		{
			lock (this)
			{
				if (m_Stop)
				{
					m_Stop = false;
					m_Event = new AutoResetEvent(false);
					m_Thread.Start();
				}
			}
		}

		public void Stop()
		{
			lock (this)
			{
				if (!m_Stop)
				{
					m_Stop = true;
					if (m_WebRequest != null) m_WebRequest.Abort();
					if (!m_Event.WaitOne(10000, false))
					{
						m_Thread.Abort();
					}
				}
			}
		}

		HttpWebRequest m_WebRequest = null;

		void ThreadEntry()
		{
			while (!m_Stop)
			{
				Byte[] buffer;
				try
				{
					if (m_Stop) break;
					String reqid = DateTime.Now.Ticks.ToString();
					m_WebRequest = HttpWebRequest.Create(Global.ServiceUrl + "/response.aspx?RequestID=" + reqid) as HttpWebRequest;
					SessionImpl.Global.WriteLog(String.Format("Post Begin: Url = response.aspx"));
					m_WebRequest.Method = "POST";
					m_WebRequest.Timeout = 2 * 60 * 1000;
					m_WebRequest.ContentType = "application/x-www-form-urlencoded";
					m_WebRequest.Headers.Add("Cookie", SessionImpl.Global.GetCookie());
					string data = String.Format("RequestID={0}&SessionID={1}&ClientMode=true&ClientVersion={2}&ServerVersion={3}", reqid, SessionImpl.Global.GetSessionID(), Assembly.GetExecutingAssembly().GetName().Version.ToString(), Global.ServerVersion);
					buffer = Encoding.UTF8.GetBytes(data);
					m_WebRequest.GetRequestStream().Write(buffer, 0, buffer.Length);
					m_WebRequest.GetRequestStream().Close();
					HttpWebResponse response = m_WebRequest.GetResponse() as HttpWebResponse;
					buffer = new byte[response.ContentLength];

					int length = 0;
					while (true)
					{
						int count = response.GetResponseStream().Read(buffer, length, (int)buffer.Length - length);
						if (count > 0) length += count;
						if (length == buffer.Length || count < 0) break;
					}
					response.Close();
				}
				catch (Exception ex)
				{
					SessionImpl.Global.WriteLog(String.Format("Post Error: Url = response.aspx, Message = {1}", m_WebRequest.RequestUri.ToString(), ex.Message));
					ReceiveResponseHandler.Global.InvokeErrorCallback("all", new Exception("服务器错误!"));
					Thread.Sleep(3000);
					continue;
				}

				try
				{
					String content = Encoding.UTF8.GetString(buffer);
					SessionImpl.Global.WriteLog(String.Format("Post Success: Url = response.aspx, ResponseText = {1}", m_WebRequest.RequestUri.ToString(), content));
					if (m_Stop) break;
					if (!String.IsNullOrEmpty(content))
					{
						Global.Desktop.BeginInvoke(new ProcessDelegate(this.Process), content);
					}
					else
					{
					}
				}
				catch
				{
				}

				Thread.Sleep(100);
			}
			m_Event.Set();
		}

		private delegate void ProcessDelegate(string content);

		private void Process(string content)
		{
			try
			{
				Utility.InvokeMethod(SessionImpl.Global.GetGlobal("ReponsesProcess"), "Process", content);
			}
			catch
			{
			}
		}
	}
}

