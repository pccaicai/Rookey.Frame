﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.Runtime.InteropServices;

namespace Rookey.Frame.IMClient
{
	public class LoadingTip : Control
	{
		protected override void OnPaint(PaintEventArgs e)
		{
			Font f = new Font("宋体", 9);
			SizeF size = e.Graphics.MeasureString(Text, f);
			e.Graphics.DrawString(Text, f, Brushes.Blue, 6, (Height - size.Height) / 2);
			base.OnPaint(e);
		}
	}
}
