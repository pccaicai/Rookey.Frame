﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;


namespace Rookey.Frame.IMClient
{
	using Properties;

	public class CustomButton : Control
	{
		BkImage m_Normal = null;
		BkImage m_Hover = null;
		BkImage m_Press = null;

		public void SetBkImage(BkImage normal, BkImage hover, BkImage press)
		{
			m_Normal = normal;
			m_Hover = hover;
			m_Press = press;
		}

		int state = 0;

		protected override void OnPaintBackground(PaintEventArgs pevent)
		{
			if (state == 0 && m_Normal != null) m_Normal.Draw(pevent.Graphics, new Rectangle(0, 0, Width, Height));
			else if (state == 1 && m_Hover != null) m_Hover.Draw(pevent.Graphics, new Rectangle(0, 0, Width, Height));
			else if (state == 2 && m_Press != null) m_Press.Draw(pevent.Graphics, new Rectangle(0, 0, Width, Height));
		}

		protected override void OnPaint(PaintEventArgs pevent)
		{}

		protected override void OnMouseDown(MouseEventArgs mevent)
		{
			if (state != 2)
			{
				state = 2;
				Refresh();
			}
			base.OnMouseDown(mevent);
		}

		protected override void OnMouseMove(MouseEventArgs mevent)
		{
			if ((mevent.Button & MouseButtons.Left) == MouseButtons.Left)
			{
				if (state != 2)
				{
					state = 2;
					Refresh();
				}
			}
			else
			{
				if (state != 1)
				{
					state = 1;
					Refresh();
				}
			}
			base.OnMouseMove(mevent);
		}

		protected override void OnMouseLeave(EventArgs e)
		{
			if (state != 0)
			{
				state = 0;
				Refresh();
			}
			base.OnMouseLeave(e);
		}
	}
}
