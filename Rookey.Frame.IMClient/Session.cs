﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Windows.Forms;

namespace Rookey.Frame.IMClient
{

	[System.Runtime.InteropServices.ComVisibleAttribute(true)]
	public class SessionImpl
	{
		Control _browser = null;
		ReceiveResponseHandler _responsesCache = null;

		public ReceiveResponseHandler ResponsesCache
		{
			get
			{
				return _responsesCache;
			}
		}

		public SessionImpl(Control browser)
		{
			_browser = browser;
			_responsesCache = new ReceiveResponseHandler(browser);
		}

		static string _username = String.Empty, _cookie = String.Empty, _sessionId = String.Empty;
		static object _userinfo = null;
		static object _lock = new object();

		public void InitService(string username, object userinfo, string cookie, string sessionId)
		{
			lock (_lock)
			{
				_username = username;
				_userinfo = userinfo;
				_cookie = cookie;
				_sessionId = sessionId;

				_responsesCache.Start();
			}

            Rookey.Frame.IMClient.Global.TrayIcon.Text = String.Format("{0}({1})", Utility.GetProperty(_userinfo, "Nickname"), _username);

			//_responsesCache.Start();
		}

		public string GetUserName()
		{
			lock (_lock)
			{
				return _username;
			}
		}

		public object GetUserInfo()
		{
			lock (_lock)
			{
				return _userinfo;
			}
		}

		public void ResetUserInfo(object userInfo)
		{
			lock (_lock)
			{
				_userinfo = userInfo;
			}
			_browser.BeginInvoke(new ResetUserInfoDelegate(UI_ResetUserInfo));
		}

		delegate void ResetUserInfoDelegate();

		void UI_ResetUserInfo()
		{
			object wm = SessionImpl.Global.GetGlobal("WindowManagement");
			Utility.InvokeMethod(wm, "Notify", "UserInfoChanged", _userinfo);
		}

		public void Reset()
		{
			_userinfo = null;
			_username = String.Empty;
			_sessionId = String.Empty;
			_cookie = String.Empty;
		}

		public string GetCookie()
		{
			lock (_lock)
			{
				return _cookie;
			}
		}

		public string GetSessionID()
		{
			lock (_lock)
			{
				return _sessionId;
			}
        }

        static object _logLock = new object();

        public void WriteLog(string text)
        {
			lock (_logLock)
			{
				try
				{
					string line = String.Format("[{0:yyyy-MM-dd HH:mm:ss}] {1}\r\n", DateTime.Now, text);
					try
					{
						if (System.IO.File.Exists(AppDomain.CurrentDomain.BaseDirectory + @"applog.txt"))
						{
							System.IO.FileInfo info = new System.IO.FileInfo(AppDomain.CurrentDomain.BaseDirectory + @"applog.txt");
							if (info.Length > 5 * 1024 * 1024) System.IO.File.Delete(AppDomain.CurrentDomain.BaseDirectory + @"applog.txt");
						}
					}
					catch
					{
					}
					System.IO.File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory + @"applog.txt", line, Encoding.UTF8);
				}
				catch
				{
				}
			}
        }

		static Hashtable m_GlobalObject = new Hashtable();

		public void RegisterGlobal(string key, object value)
		{
			lock (m_GlobalObject)
			{
				m_GlobalObject[key.ToUpper()] = value;
			}
		}

		public void RemoveGlobal(string key)
		{
			lock (m_GlobalObject)
			{
				m_GlobalObject.Remove(key.ToUpper());
			}
		}

		public object GetGlobal(string key)
		{
			lock (m_GlobalObject)
			{
				return m_GlobalObject[key.ToUpper()];
			}
		}

		static SessionImpl m_Global = new SessionImpl(null);

		static public SessionImpl Global { get { return m_Global; } }
	}
}
