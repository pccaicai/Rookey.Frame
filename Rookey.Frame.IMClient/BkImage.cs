﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

namespace Rookey.Frame.IMClient
{
	public class BkImage
	{
		Image _image = null;
		Rectangle _bounds = Rectangle.Empty;
		int _splitLeft, _splitRight, _splitTop, _splitBottom;
		bool _isFixed = true;
		Rectangle[] _srcRects = new Rectangle[9];

		public BkImage(Image img, Rectangle bounds)
		{
			_image = img;
			_bounds = bounds;
			_isFixed = true;
		}

		public BkImage(Image img, Rectangle bounds, int splitX1, int splitX2, int splitY1, int splitY2)
			: this(img, bounds)
		{
			_splitLeft = splitX1;
			_splitRight = splitX2;
			_splitTop = splitY1;
			_splitBottom = splitY2;
			_isFixed = false;

			int[] xs = new int[] { 0, _splitLeft, img.Width - _splitRight, img.Width };
			int[] ys = new int[] { 0, _splitTop, img.Height - _splitBottom, img.Height };

			for (int y = 0; y < 3; y++)
			{
				for (int x = 0; x < 3; x++)
				{
					int i = y * 3 + x;
					_srcRects[i] = new Rectangle(xs[x], ys[y], xs[x + 1] - xs[x], ys[y + 1] - ys[y]);
				}
			}
		}

		public void Draw(Graphics graphics, Rectangle range)
		{
			if (_isFixed)
			{
				graphics.DrawImage(_image, range, _bounds, GraphicsUnit.Pixel);
			}
			else
			{

				int[] xs = new int[] { 0, _splitLeft, range.Width - _splitRight, range.Width };
				int[] ys = new int[] { 0, _splitTop, range.Height - _splitBottom, range.Height };

				for (int y = 0; y < 3; y++)
				{
					for (int x = 0; x < 3; x++)
					{
						int i = y * 3 + x;
						Rectangle desRect = new Rectangle(xs[x], ys[y], xs[x + 1] - xs[x], ys[y + 1] - ys[y]);
						graphics.DrawImage(_image, desRect, _srcRects[i], GraphicsUnit.Pixel);
					}
				}
			}
		}
	}
}
