﻿/*----------------------------------------------------------------
        // Copyright (C) 2016 Rookey
        // 版权所有
        // 开发者：Rookey
        // Email：rookey@yeah.net
        // QQ：3319549098
//----------------------------------------------------------------*/

using Rookey.Frame.EntityBase;
using Rookey.Frame.EntityBase.Attr;
using ServiceStack.DataAnnotations;
using System;

namespace Rookey.Frame.Model.Bpm
{
    /// <summary>
    /// 流程连线实例
    /// </summary>
    [ModuleConfig(Name = "连线实例", PrimaryKeyFields = "Bpm_WorkFlowInstanceId,Bpm_WorkLineId,Bpm_WorkNodeInstanceStartId,Bpm_WorkNodeInstanceEndId", TitleKey = "SerialNo", Sort = 78, StandardJsFolder = "Bpm")]
    public class Bpm_WorkLineInstance : BaseBpmEntity
    {
        /// <summary>
        /// 流水号
        /// </summary>
        [FieldConfig(Display = "流水号", RowNum = 1, ColNum = 1, IsRequired = true, IsUnique = true, HeadSort = 1)]
        public int SerialNo { get; set; }

        /// <summary>
        /// 流程实例Id
        /// </summary>
        [FieldConfig(Display = "流程实例", ControlType = (int)ControlTypeEnum.DialogGrid, IsRequired = true, RowNum = 1, ColNum = 2, HeadSort = 2, ForeignModuleName = "流程实例")]
        public Guid? Bpm_WorkFlowInstanceId { get; set; }

        /// <summary>
        /// 流程实例
        /// </summary>
        [Ignore]
        public string Bpm_WorkFlowInstanceName { get; set; }

        /// <summary>
        /// 连线Id
        /// </summary>
        [FieldConfig(Display = "流程连线", ControlType = (int)ControlTypeEnum.DialogGrid, IsRequired = true, RowNum = 2, ColNum = 1, HeadSort = 3, ForeignModuleName = "流程连线")]
        public Guid? Bpm_WorkLineId { get; set; }

        /// <summary>
        /// 流程连线
        /// </summary>
        [Ignore]
        public string Bpm_WorkLineName { get; set; }

        /// <summary>
        /// 起始结点实例Id
        /// </summary>
        [FieldConfig(Display = "起始结点实例", ControlType = (int)ControlTypeEnum.DialogGrid, IsRequired = true, RowNum = 2, ColNum = 2, HeadSort = 4, ForeignModuleName = "节点实例")]
        public Guid? Bpm_WorkNodeInstanceStartId { get; set; }

        /// <summary>
        /// 终止结点实例Id
        /// </summary>
        [FieldConfig(Display = "终止结点实例", ControlType = (int)ControlTypeEnum.DialogGrid, IsRequired = true, RowNum = 3, ColNum = 1, HeadSort = 5, ForeignModuleName = "节点实例")]
        public Guid? Bpm_WorkNodeInstanceEndId { get; set; }
    }
}
