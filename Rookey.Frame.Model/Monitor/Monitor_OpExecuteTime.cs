﻿/*----------------------------------------------------------------
        // Copyright (C) 2016 Rookey
        // 版权所有
        // 开发者：Rookey
        // Email：rookey@yeah.net
        // QQ：3319549098
//----------------------------------------------------------------*/

using Rookey.Frame.EntityBase.Attr;

namespace Rookey.Frame.Model.Monitor
{
    /// <summary>
    /// 操作时间监控
    /// </summary>
    [ModuleConfig(Name = "操作时间监控", PrimaryKeyFields = "ModuleName,ControllerName,ActionName,CreateDate", Sort = 60, IsAllowAdd = false, IsAllowEdit = false, IsAllowExport = true, StandardJsFolder = "Monitor")]
    public class Monitor_OpExecuteTime : BaseMonitorEntity
    {
        /// <summary>
        /// 模块名称
        /// </summary>
        [FieldConfig(Display = "模块名称", IsFormVisible = false, HeadSort = 1)]
        public string ModuleName { get; set; }

        /// <summary>
        /// 控制器名称
        /// </summary>
        [FieldConfig(Display = "控制器名称", IsFormVisible = false, HeadSort = 2)]
        public string ControllerName { get; set; }

        /// <summary>
        /// Action名称
        /// </summary>
        [FieldConfig(Display = "Action名称", IsFormVisible = false, HeadSort = 3)]
        public string ActionName { get; set; }

        /// <summary>
        /// 执行时间（毫秒）
        /// </summary>
        [FieldConfig(Display = "执行时间", IsFormVisible = false, HeadSort = 4)]
        public double ExecuteMiniSeconds { get; set; }

        /// <summary>
        /// 操作用户
        /// </summary>
        [FieldConfig(Display = "操作用户", IsFormVisible = false, HeadSort = 5)]
        public string OpUserName { get; set; }

        /// <summary>
        /// 客户端IP
        /// </summary>
         [FieldConfig(Display = "客户端IP", IsFormVisible = false, HeadSort = 6)]
        public string ClientIp { get; set; }
    }
}
