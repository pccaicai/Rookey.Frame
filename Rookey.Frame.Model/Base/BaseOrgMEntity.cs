﻿/*----------------------------------------------------------------
        // Copyright (C) 2016 Rookey
        // 版权所有
        // 开发者：Rookey
        // Email：rookey@yeah.net
        // QQ：3319549098
//----------------------------------------------------------------*/
using Rookey.Frame.EntityBase;

namespace Rookey.Frame.Model
{
    /// <summary>
    /// 组织管理基类
    /// </summary>
    public class BaseOrgMEntity : BaseEntity
    {
    }
}
