﻿/*----------------------------------------------------------------
        // Copyright (C) 2016 Rookey
        // 版权所有
        // 开发者：Rookey
        // Email：rookey@yeah.net
        // QQ：3319549098
//----------------------------------------------------------------*/

using Rookey.Frame.Common;
using Rookey.Frame.Model.Sys;
using Rookey.Frame.Operate.Base;
using Rookey.Frame.Operate.Base.EnumDef;
using System.Collections.Generic;
using System.Web;
using System.Linq;
using System;

namespace Rookey.Frame.UIOperate
{
    /// <summary>
    /// UI操作
    /// </summary>
    public static class UIOperate
    {
        /// <summary>
        /// 从request中取moduleId
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static Guid GetModuleIdByRequest(HttpRequestBase request)
        {
            Sys_Module module = SystemOperate.GetModuleByRequest(request);
            if (module != null)
                return module.Id;
            return Guid.Empty;
        }

        /// <summary>
        /// 获取网格HTML
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static string GetGridHTML(HttpRequestBase request)
        {
            UIFrameFactory frameFactory = UIFrameFactory.GetInstance();
            string condition = string.Empty; //条件参数
            Guid? viewId = null; //视图Id
            DataGridType gridType = DataGridType.MainGrid;
            string initModule = string.Empty;
            string initField = string.Empty;
            Dictionary<string, object> dic = null; //网格其他参数
            Guid moduleId = UIOperate.GetModuleIdByRequest(request); //模块Id
            string page = request["page"].ObjToStr(); //页面类型
            condition = request["condition"].ObjToStr(); //条件参数
            viewId = request["viewId"].ObjToGuidNull(); //视图Id
            bool recycle = request["recycle"].ObjToInt() == 1; //是否回收站
            bool draft = request["draft"].ObjToInt() == 1; //我的草稿
            gridType = DataGridType.MainGrid;
            initModule = string.Empty;
            initField = string.Empty;
            if (page == "fdGrid") //弹出网格
            {
                gridType = DataGridType.DialogGrid;
            }
            else if (page == "fwGrid") //列表页面明细或附属模块网格
            {
                gridType = DataGridType.FlowGrid;
            }
            else if (page == "inGrid") //网格内嵌入网格
            {
                gridType = DataGridType.InnerDetailGrid;
            }
            else if (recycle) //回收站网格
            {
                gridType = DataGridType.RecycleGrid;
            }
            else if (draft) //我的草稿网格
            {
                gridType = DataGridType.MyDraftGrid;
            }
            if (gridType == DataGridType.DialogGrid)
            {
                initModule = HttpUtility.UrlDecode(request["initModule"].ObjToStr());
                initField = request["initField"].ObjToStr();
            }
            //where条件语句
            string where = request["where"].ObjToStr();
            if (!string.IsNullOrWhiteSpace(where))
            {
                try
                {
                    where = MySecurity.DecodeBase64(HttpUtility.UrlDecode(where));
                }
                catch
                {
                    where = string.Empty;
                }
            }
            //过滤字段
            List<string> filterFieldsList = null;
            string filterFields = request["filterFields"].ObjToStr();
            if (!string.IsNullOrWhiteSpace(filterFields))
            {
                filterFieldsList = filterFields.Split(",".ToCharArray(), System.StringSplitOptions.RemoveEmptyEntries).ToList();
            }
            //提出网格参数
            dic = request.Params.ToDictionary().Where(x => x.Key.StartsWith("p_")).ToDictionary();
            string mutiSelect = request["ms"].ObjToStr(); //启用多选
            if (mutiSelect == "1") dic.Add("muti_select", true);
            Guid? menuId = request["mId"].ObjToGuidNull(); //菜单ID
            return frameFactory.GetGridHTML(moduleId, gridType, condition, where, viewId, initModule, initField, dic, false, filterFieldsList, menuId);
        }

        /// <summary>
        /// 是否启用IM功能
        /// </summary>
        /// <returns></returns>
        public static bool IsEnableIM()
        {
            return SystemOperate.IsEnableIM();
        }
    }
}
