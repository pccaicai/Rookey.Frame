﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rookey.Frame.IMCore
{
	public class Message : IRenderJson
	{
		public AccountInfo Sender, Receiver;
		public String Content;
		public DateTime CreatedTime;
		public Int64 Key;

		public Message(
			AccountInfo sender, AccountInfo receiver,
			String content, DateTime cteatedTime, Int64 key
		)
		{
			Sender = sender;
			Receiver = receiver;
			Content = content;
			CreatedTime = cteatedTime;
			Key = key;
		}

		void IRenderJson.RenderJson(StringBuilder builder)
		{
			Utility.RenderHashJson(
				builder,
				"Sender", Sender,
				"Receiver", Receiver,
				"CreatedTime", CreatedTime,
				"Key", Key,
				"Content", Content
			);
		}
	}

	public interface IMessageStorage
	{
		Int64 GetMaxKey();
		DateTime GetCreatedTime();
		List<Message> FindHistory(long user, long peer, DateTime from, DateTime to);
		List<Message> Find(long receiver, long sender, Nullable<DateTime> from);
		void Write(List<Message> messages);
	}
}
